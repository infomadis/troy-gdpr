# GDPR made easy for engineers

Java annotations for fields and classes that process personally identifiable information. 

Automatically generate reports about process inventories to comply with GDPR regulations.


You just need to do three things:

1. add @Gdpr annotations to relevant fields (e.g. in a class UserInfo)

```java
    @Gdpr(legalBase = { GdprLawfulBasis.CONSENT }, description = "The voluntarily entered email address of the user")
    private String email;
```

1. add @ProcessingOperation annotations to classes processing these fields

```java
    @ProcessingOperation(legalBase = {GdprLawfulBasis.CONTRACT_FULFILLMENT}, 
        purpose = "Sending email to users", 
        description = "Mails are sent to all users registered for the newsletter", 
        references = UserInfo.class)
    public class EmailSender {
        // ...
    }
```

1. add the annotation processor

```xml
<configuration>
  <annotationProcessorPaths>
    <path>
      <groupId>de.troy-bleiben</groupId>
      <artifactId>gdprr</artifactId>
      <version>${troygdpr.version}</version>
    </path>
  </annotationProcessorPaths>
</configuration>
```

Or for Kotlin

```xml
            <plugin>
                <groupId>org.jetbrains.kotlin</groupId>
                <artifactId>kotlin-maven-plugin</artifactId>
                <version>${kotlin.version}</version>
                <executions>
                    <execution>
                        <id>kapt</id>
                        <goals>
                            <goal>kapt</goal>
                        </goals>
                        <configuration>
                            <sourceDirs>
                                <sourceDir>src/main/kotlin</sourceDir>
                                <sourceDir>src/main/java</sourceDir>
                            </sourceDirs>
                            <annotationProcessorPaths>
                                <annotationProcessorPath>
                                    <groupId>de.troy-bleiben</groupId>
                                    <artifactId>gdpr</artifactId>
                                    <version>1.0-SNAPSHOT</version>
                                </annotationProcessorPath>
                            </annotationProcessorPaths>
                        </configuration>
                    </execution>
                    <execution>
                        <id>compile</id>
                        <goals>
                            <goal>compile</goal>
                        </goals>
                        <configuration>
                            <sourceDirs>
                                <sourceDir>${project.basedir}/src/main/kotlin</sourceDir>
                                <sourceDir>${project.basedir}/src/main/java</sourceDir>
                            </sourceDirs>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
```
