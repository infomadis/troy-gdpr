package de.infomadis.gdpr

/**
 * This enum determines the data subject (in the sense of GDPR) for which the data field applies.
 */
enum class DataSubject {
    /**
     * Potential customer or other user of the data controller with passing contact (e.g. visitor of the website).
     */
    USER,
    /**
     * Customer of the data controller (i.e. person with a contractual relationship).
     */
    CUSTOMER,
    /**
     * Employee of the data controller or processor.
     */
    EMPLOYEE,
    /**
     * Client (contact or other natural person representing a client.
     */
    CLIENT,
    /**
     * Any other natural person (mostly for cases where it is not clear; not that multiple data subjects can be annotated).
     */
    OTHER_NATURAL,
    /**
     * Any other person-like data subject.
     */
    OTHER,
    /**
     * To be used when the annotation makes sense to use for fields that are not actually related to persons e.g. for comment purposes.
     */
    NOT_APPLICABLE,
}