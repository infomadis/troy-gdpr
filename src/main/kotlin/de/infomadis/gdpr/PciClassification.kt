package de.infomadis.gdpr

/**
 * This enum classifies and marks data fields according to relevance for PCI DSS compliance.
 */
enum class PciClassification(val pciRelevant: Boolean?) {
    /**
     * The PCI classification is unknown.
     */
    UNKNOWN(null),
    /**
     * Should be used to indicate that the PCI classification has not been evaluated (yet).
     */
    NOT_EVALUATED(null),
    /**
     * Can be used for fields that do not fall under PCI, but are for some reason tagged too (e.g. for completeness or comments or historical changes).
     */
    NOT_APPLICABLE(false),
    /**
     * Should be used for credit card data fields .
     */
    CREDIT_CARD_DATA(true),
    /**
     * Can be used to specifically denote fields that hold CVC data. These must be transient.
     */
    CVC_CODE(true),
    /**
     * May be used for bank account data fields.
     */
    BANK_ACCOUNT_DATA(true),
    /**
     * May be used for other data fields that hold personally identifiably data comparable to credit card data but not falling under PCI regulations.
     */
    PERSONALLY_IDENTIFIABLE_DATA(false),
}