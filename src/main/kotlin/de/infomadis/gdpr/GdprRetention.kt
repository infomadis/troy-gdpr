package de.infomadis.gdpr

/**
 * @param retentionDays days the personally identifiable data may be retained at the place in question
 */
@MustBeDocumented
@Target(AnnotationTarget.FIELD, AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention(AnnotationRetention.RUNTIME)
annotation class GdprRetention(val retentionDays: Int) {
}