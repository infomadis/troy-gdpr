package de.infomadis.gdpr

/**
 * Fields not tagged with this annotation are assumed to not be relevant for PCI DSS compliance
 * @param pciClassification applicable for this field
 * @param comment a comment explaining why how this was determined (e.g. ticket number, file name, link to doc etc.)
 */
@MustBeDocumented
@Target(AnnotationTarget.FIELD, AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention(AnnotationRetention.SOURCE)
annotation class PciCreditCardData(val pciClassification:PciClassification, val comment: String) {
}