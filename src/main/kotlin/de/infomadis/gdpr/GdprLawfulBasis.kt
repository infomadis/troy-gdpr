package de.infomadis.gdpr

/**
 * This enum makes reasons for processing of personally identifiable (PII) data according to GDPR explicit.
 */
enum class GdprLawfulBasis(val compliant: Boolean) {
    /**
     * The legal basis is unknown. This can be used to indicate that the applicability of GDPR is unknown or that it may be known to the controller but not the processor.
     */
    UNKNOWN(false),
    /**
     * Should be used to indicate that the legal basis has not been evaluated (yet).
     */
    NOT_EVALUATED(false),
    /**
     * Can be used for fields that do not fall under GDPR, but are for some reason tagged too (e.g. for completeness).
     */
    NOT_APPLICABLE(true),
    /**
     * Should be used for fields that are (currently) processed but for which it is known that they shouldn't.
     */
    NOT_COMPLIANT(false),
    /**
     * May be used for cases where the processing is carried out by entities that don't fall within the jurisdiction of the GDPR.
     */
    NOT_WITHIN_EU(true),
    /**
     * Can be used for fields that are derived (temporarily) from other fields (which may or may not fall under GDPR.
     */
    DERIVED_INFORMATION(true),
    /**
     * The person explicitly consented to the processing of their data.
     * Art. 6 §1 a) of the conditions for valid lawful processing.
     */
    CONSENT(true),
    /**
     * The data is needed to fulfill a contract between the person and the controller.
     * Art. 6 §1 b) of the conditions for valid lawful processing.
     */
    CONTRACT_FULFILLMENT(true),
    /**
     * The processor or controller of the PIIs has a legal obligation to fulfill.
     * Art. 6 §1 c) of the conditions for valid lawful processing.
     */
    LEGAL_COMPLIANCE(true),
    /**
     * Needed for the vital interests of the data subject.
     * Art. 6 §1 d) of the conditions for valid lawful processing.
     */
    VITAL_INTERESTS_OF_SUBJECT(true),
    /**
     * Needed for the fulfillment of a task in the public interest.
     * Art. 6 §1 e) of the conditions for valid lawful processing.
     */
    PUBLIC_INTEREST(true),
    /**
     * The processor or controller of the PIIs has a legitimate interest in the processing of the data.
     * Art. 6 §1 f) of the conditions for valid lawful processing.
     */
    LEGITIMATE_INTEREST(true),
}

// the following are examples of statutory sources mandating specific processing.

/**
 * https://de.wikipedia.org/wiki/Aufbewahrungsfrist
 * e.g. §238,§257 HGB https://www.buzer.de/s1.htm?g=HGB&a=238,257
 * §147 AO https://www.gesetze-im-internet.de/ao_1977/__147.html
 * §4 LStDV https://www.gesetze-im-internet.de/lstdv/__4.html
 * §26 BDSG https://www.gesetze-im-internet.de/bdsg_2018/__26.html
 */
val COMPULSORY_RECORD_RETENTION = "COMPULSORY_RECORD_RETENTION"
/**
 * §88 DSGVO https://dsgvo-gesetz.de/art-88-dsgvo/
 * §26 BDSG https://www.gesetze-im-internet.de/bdsg_2018/__26.html
 */
val EMPLOYEE_DATA_PROCCESSING = "EMPLOYEE_DATA_PROCCESSING"
/**
 * §26 BDSG https://www.gesetze-im-internet.de/bdsg_2018/__26.html
 */
val ACCESS_CONTROL = "ACCESS_CONTROL"
/**
 * §7 UWG https://www.gesetze-im-internet.de/uwg_2004/__7.html
 */
val LEGITIMATE_MARKETING = "LEGITIMATE_MARKETING"
/**
 * §28 GDPR
 */
val PROCESSING_AS_PROCESSOR = "PROCESSING_AS_PROCESSOR"
