package de.infomadis.gdpr

/**
 * This annotation can be used on a class to indicate the location.
 * @param location the name of the system where the data is stored (
 * @param type the type of data storage (database, cache, transport protocol, frontend)
 * @param comment a comment explaining why how this was determined (e.g. ticket number, file name, link to doc etc.)
 */
@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.SOURCE)
annotation class GdprLocation(val location: String, val comment: String = "", val type: String = "database") {
}