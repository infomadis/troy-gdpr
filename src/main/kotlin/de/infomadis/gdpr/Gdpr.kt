package de.infomadis.gdpr

/**
 * @param legalBase a set of legal bases for processing. Should at least contain one entry, e.g. UNKNOWN
 * @param comment a comment explaining why how this was determined (e.g. ticket number, file name, link to doc etc.)
 * @param subject the data subject for which the data is stored, optional, intended to be given if the subject is not the default subject (e.g. customer) but another data subject, e.g. employee. It is recommended to use constants for these.
 */
@MustBeDocumented
@Target(AnnotationTarget.FIELD, AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention(AnnotationRetention.SOURCE)
annotation class Gdpr(val legalBase:Array< GdprLawfulBasis>, val comment: String, val subject: String = "<default data subject>") {
}

