package de.infomadis.gdpr

import kotlin.reflect.KClass

/**
 * https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX:31995L0046:en:html
 * The name is inferred from the class name this annotation is attached to
 * @param legalBase a set of legal bases for processing according to GDPR . Should at least contain one entry, e.g. UNKNOWN
 * @param statutorySource a set of specific legal texts regulating the processing, see []LegalCompliance] for references
 * @param purpose short description of the purpose of the processing
 * @param description description of the processing
 * @param impactAssessment see https://dsgvo-vorlagen.de/muster-fuer-eine-datenschutz-folgenabschaetzung-dsfs-nach-dsgvo
 * @param url link to further documentation
 * @param comment a comment explaining why how this was determined (e.g. ticket number, file name, link to doc etc.)
 * @param specialPiis
 * @param subject the data subject, optional, is inferred from the fields processed by the annotated class
 */
@MustBeDocumented
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.SOURCE)
annotation class ProcessingOperation(val legalBase: Array<GdprLawfulBasis>,
                                     val statutorySource: Array<String> = [],
                                     val purpose: String,
                                     val description: String,
                                     val impactAssessment: String = "",
                                     val url: String = "",
                                     val comment: String = "",
                                     val specialPiis: Boolean = false,
                                     val references: Array<KClass<*>> = [],
                                     val subject: String = "<default data subject>") {
}
