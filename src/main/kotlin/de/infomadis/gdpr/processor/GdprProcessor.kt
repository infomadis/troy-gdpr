package de.infomadis.gdpr.processor

import com.opencsv.bean.StatefulBeanToCsvBuilder
import com.sun.source.util.Trees
import de.infomadis.gdpr.Gdpr
import de.infomadis.gdpr.ProcessingOperation
import java.io.File
import java.io.OutputStreamWriter
import javax.annotation.processing.*
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.TypeElement
import javax.lang.model.element.VariableElement
import javax.lang.model.type.DeclaredType
import javax.lang.model.type.MirroredTypesException
import javax.lang.model.type.TypeMirror
import javax.lang.model.util.SimpleTypeVisitor9
import javax.tools.Diagnostic
import javax.tools.StandardLocation


@SupportedAnnotationTypes(value = ["de.infomadis.gdpr.Gdpr",
    "de.infomadis.gdpr.ProcessingOperation"/*,
    "de.infomadis.gdpr.GdprRetention",
    "de.infomadis.gdpr.GdprSubject",
    "de.infomadis.gdpr.PciCreditCardData"*/])
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedOptions(GdprProcessor.KAPT_KOTLIN_GENERATED_OPTION_NAME)
class GdprProcessor : AbstractProcessor() {
    companion object {
        const val KAPT_KOTLIN_GENERATED_OPTION_NAME = "kapt.kotlin.generated"
    }

    var trees: Trees? = null

    override fun init(processingEnv: ProcessingEnvironment) {
        super.init(processingEnv)
        trees = Trees.instance(processingEnv)
    }

    val project = File(System.getProperty("user.dir")).canonicalFile.name

    override fun process(annotations: Set<TypeElement>, roundEnv: RoundEnvironment): Boolean {


        val gdprFields = processGDPRAnnotations(annotations, roundEnv)
        val relations = mutableListOf<Pair<String, String>>()
        val procOps = processProcessingOperations(annotations, relations, roundEnv)
        relations.addAll(processRelations(annotations, roundEnv))

        if (gdprFields.size > 0) {
            writeResults(gdprFields, procOps, relations)
        } else {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, "no entries found for $annotations")
        }

        return true
    }

    fun processGDPRAnnotations(annotations: Set<TypeElement>, roundEnv: RoundEnvironment): Collection<GdprLine> {

        val gdprAnnotation = annotations.find { it.simpleName.toString() == "Gdpr" } ?: return emptyList()

        return roundEnv.getElementsAnnotatedWith(gdprAnnotation).map { element ->

            //val gdprRetention = element.getAnnotation(GdprRetention::class.java)
            //val gdprSubject = element.getAnnotation(GdprSubject::class.java)

            val gdpr = element.getAnnotation(Gdpr::class.java)
            val reasons = gdpr.legalBase.map { it.name }.joinToString(",")
            val pkg = processingEnv.elementUtils.getPackageOf(element).qualifiedName.toString()
            val subject = if (gdpr.subject == "<default data subject>") "CUSTOMER" else gdpr.subject
            val className = element.enclosingElement.simpleName.toString()
            val fieldName = element.simpleName.toString()

            processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "found @Gdpr at $pkg.$className.$fieldName")
            GdprLine(project, fieldName,
                    className,
                    pkg,
                    subject,
                    reasons,
                    gdpr.comment)
        }
    }

    fun processProcessingOperations(annotations: Set<TypeElement>, relations: MutableCollection<Pair<String, String>>, roundEnv: RoundEnvironment): Collection<ProcOpLine> {
        val procAnnotation = annotations.find { it.simpleName.toString() == "ProcessingOperation" }
                ?: return emptyList()

        return roundEnv.getElementsAnnotatedWith(procAnnotation).map { element ->
            val procOp = element.getAnnotation(ProcessingOperation::class.java)
            val reasons = procOp.legalBase.map { it.name }.joinToString(",")
            val pkg = processingEnv.elementUtils.getPackageOf(element).qualifiedName.toString()
            val className = element.simpleName.toString()
            val subject = if (procOp.subject == "<default data subject>") "CUSTOMER" else procOp.subject

            val ident = "$pkg.$className"

            var refs: List<TypeMirror>
            try {
                procOp.references // this should throw
                throw IllegalStateException()
            } catch (mte: MirroredTypesException) {
                refs = mte.typeMirrors
            }

            for (refMirrow in refs) {
                val ref = asTypeElement(refMirrow)
                val pkg = processingEnv.elementUtils.getPackageOf(ref).qualifiedName.toString()
                relations.add(ident to "$pkg.${ref.simpleName}")
            }

            processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "found @ProcessingOperation at $pkg.$className")
            ProcOpLine(project = project,
                    className = className,
                    pkg = pkg,
                    subject = subject,
                    reasons = reasons,
                    comment = procOp.comment,
                    purpose = procOp.purpose,
                    description = procOp.description,
                    impactAssessment = procOp.impactAssessment,
                    specialPiis = procOp.specialPiis)
        }
    }

    fun processRelations(annotations: Set<TypeElement>, roundEnv: RoundEnvironment): Collection<Pair<String, String>> {
        val procAnnotation = annotations.find { it.simpleName.toString() == "ProcessingOperation" }
                ?: return emptyList()

        return roundEnv.getElementsAnnotatedWith(procAnnotation).flatMap { element ->

            val pkg = processingEnv.elementUtils.getPackageOf(element).qualifiedName.toString()
            val className = element.simpleName.toString()
            val ident = "$pkg.$className"

            findRelationsInElement(element, ident)
        }
    }

    private fun writeResults(annots: Collection<GdprLine>, procOps: Collection<ProcOpLine>, relations: Collection<Pair<String, String>>) {

        processingEnv.filer.createResource(StandardLocation.SOURCE_OUTPUT, "de.troy.gdpr", "gdpr-annotations.csv")
                .apply {
                    OutputStreamWriter(openOutputStream()).use {
                        StatefulBeanToCsvBuilder<GdprLine>(it).build().write(annots.toMutableList())
                    }
                }
        processingEnv.filer.createResource(StandardLocation.SOURCE_OUTPUT, "de.troy.gdpr", "gdpr-processing-operations.csv").apply {
            OutputStreamWriter(this.openOutputStream()).use {
                StatefulBeanToCsvBuilder<ProcOpLine>(it).build().write(procOps.toMutableList())
            }
        }
        val relFiltered = relations.filter {
            !it.second.startsWith("java")
        }
        processingEnv.filer.createResource(StandardLocation.SOURCE_OUTPUT, "de.troy.gdpr", "gdpr-xref.csv").apply {
            OutputStreamWriter(this.openOutputStream()).use {
                StatefulBeanToCsvBuilder<Pair<String, String>>(it).build().write(relFiltered)
            }
        }
        val refs = relations.map { it.second }
        val unreferenced = annots.filter { !refs.contains(it.identifier()) }
        processingEnv.filer.createResource(StandardLocation.SOURCE_OUTPUT, "de.troy.gdpr", "unreferenced.csv").apply {
            OutputStreamWriter(this.openOutputStream()).use {
                it.write(unreferenced.joinToString("\n"))
            }
        }


        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "wrote ${annots.size} csv entries")
    }

    private fun findRelationsInElement(element: Element, ident: String, visited: MutableSet<Any> = mutableSetOf())
            : List<Pair<String, String>> {
        if (visited.contains(element)) return emptyList()
        visited.add(element)
        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "visit scope ${element} ${element.kind}")
        val subrelations = mutableListOf<Pair<String, String>>()
        if (element is TypeElement)
            subrelations.add(findRelationsInType(element, ident))
        if (element is VariableElement)
            subrelations.addAll(findRelationsInElement(element, ident, visited))
        for (enclosedElement in element.getEnclosedElements()) {
            /*
            if (enclosedElement is javax.lang.model.element.ExecutableElement) {
                val methodTree = trees!!.getTree(enclosedElement)
                if (methodTree == null)
                    processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "no method tree in $enclosedElement")
                else {
                    processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "visit exec $enclosedElement")
                    val blockTree = methodTree.getBody()
                    for (statementTree in blockTree.getStatements()) {
                        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "found $statementTree")

                        // *do something with the statements*
                    }
                }
            }
            */
            if (enclosedElement is VariableElement) {
                subrelations.addAll(findRelationsInVariable(enclosedElement.asType(), ident, visited))
            }
            subrelations.addAll(findRelationsInElement(enclosedElement, ident, visited))
        }
        return subrelations
    }

    fun findRelationsInVariable(element: TypeMirror, ident: String, visited: MutableSet<Any>): Collection<Pair<String, String>> {
        if (visited.contains(element)) return emptyList()
        visited.add(element)
        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "visit var ${element} ${element.kind}")
        val relations = mutableListOf<Pair<String, String>>()
        element.accept(object : SimpleTypeVisitor9<Unit, Unit>() {
            override fun visitDeclared(t: DeclaredType, p: Unit?) {
                super.visitDeclared(t, p)
                t.enclosingType?.let { relations.addAll(findRelationsInVariable(it, ident, visited)) }
                relations.addAll(findRelationsInVariable(t, ident, visited))
                for (ta in t.typeArguments)
                    relations.addAll(findRelationsInVariable(ta, ident, visited))
                relations.add(findRelationsInType(t.asElement() as TypeElement, ident))
            }
        }, Unit)
        return relations
    }


    fun findRelationsInType(element: TypeElement, procOp: String): Pair<String, String> {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "visit type ${element} ${element.kind}")
        val pkg = processingEnv.elementUtils.getPackageOf(element).qualifiedName.toString()
        return procOp to "$pkg.${element.simpleName}"
    }

    private fun asTypeElement(typeMirror: TypeMirror): TypeElement {
        val TypeUtils = this.processingEnv.getTypeUtils()
        return TypeUtils.asElement(typeMirror) as TypeElement
    }

    data class GdprLine(val project: String, val fieldName: String, val className: String, val pkg: String, val subject: String, val reasons: String, val comment: String) {
        fun identifier() = "$pkg.$className.$fieldName"
    }

    data class ProcOpLine(val project: String, val className: String, val pkg: String, val subject: String, val reasons: String, val comment: String,
                          val purpose: String,
                          val description: String,
                          val impactAssessment: String,
                          val specialPiis: Boolean)
}

