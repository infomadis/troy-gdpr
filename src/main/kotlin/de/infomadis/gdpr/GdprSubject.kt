package de.infomadis.gdpr

/**
 * This annotation can be used on a container or class to indicate the default data subject.
 * @param subject a set of DataSubjects. Should at least contain one entry.
 * @param comment a comment explaining why how this was determined (e.g. ticket number, file name, link to doc etc.)
 */
@MustBeDocumented
@Target(AnnotationTarget.FIELD, AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.SOURCE)
annotation class GdprSubject(val subject:Array< DataSubject>, val comment: String = "") {
}