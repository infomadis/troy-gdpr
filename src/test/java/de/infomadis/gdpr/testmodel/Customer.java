package de.infomadis.gdpr.testmodel;

import de.infomadis.gdpr.Gdpr;

public class Customer {

    @Gdpr(legalBase = {},comment = "The persons name")
    private String name;

    @Gdpr(legalBase = {},comment = "Birthday")
    private String birthday;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

}
