package de.infomadis.gdpr

import com.google.testing.compile.Compilation
import com.google.testing.compile.Compiler.javac
import com.google.testing.compile.JavaFileObjects
import de.infomadis.gdpr.processor.GdprProcessor
import org.junit.Test
import kotlin.test.assertEquals

class GdprProcessorTest {


    @Test
    fun testProcessor(){
        val compilation =
             javac()
                 .withProcessors(GdprProcessor())
                 .compile(
                         JavaFileObjects.forResource("de/infomadis/gdpr/testmodel/Customer.java")
                 )

        assertEquals(Compilation.Status.SUCCESS, compilation.status())

    }

}